package br.com.thiagopagotto.fan.resources;

import br.com.thiagopagotto.fan.model.Fan;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FanResource {

    @RequestMapping("/fan/{id}")
    public Fan getFan(@PathVariable String id){
        return new Fan(id, "Thiago Pagotto","pagottoo@gmail.com");
    }

}
