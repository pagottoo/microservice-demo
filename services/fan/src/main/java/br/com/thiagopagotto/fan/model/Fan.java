package br.com.thiagopagotto.fan.model;


public class Fan {

    private final String id;

    private final String fullName;

    private final String email;

    public Fan(String id, String fullName, String email) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

}
