package br.com.thiagopagotto.campaign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CampaignService {

    public static void main(String[] args) {
        SpringApplication.run(CampaignService.class, args);
    }

}
