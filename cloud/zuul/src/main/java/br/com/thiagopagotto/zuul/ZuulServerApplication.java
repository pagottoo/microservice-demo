package br.com.thiagopagotto.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import br.com.thiagopagotto.zuul.filters.LoggingPreFilter;

@SpringBootApplication
@EnableZuulProxy
public class ZuulServerApplication {

    public static void main(String[] args){
        SpringApplication.run(ZuulServerApplication.class, args);
    }

    @Bean
    public LoggingPreFilter loggingPreFilter(){
        return new LoggingPreFilter();
    }

}
