microservice-demo
===============

Este projeto é minha visão sobre arquitetura de micro serviços em Java usando o Netflix OSS e Spring Cloud como frameworks.

Overview:
Todos os micro serviços sobem em portas randômicas e se registram no Eureka.
O Eureka sobe na porta 8761 e exibe os serviços registrados nele.
A API sobe na porta 8080, e expõe os serviços, cada serviço chama seu dataprovider, que usa um loadbalancer e faz o request para o micro serviço.
O Turbine expõe a porta 8989 e ao acessar /hystrix e informar a url do Turbine, ou do Zuul vemos um monitoramento dos micro serviços.
Ja o Zuul faz a parte de roteamento dinamico das requests atuando como gateway dos micro serviços. Nesse ponto eu gostaria de usar o KONG da MASHAPE, porém ele exigiria uma complexidade maior para avaliar esta POC.

O projeto está separado nos seguintes módulos:

 - api (api)
     - /campaign
     - /fan
     - /team
 - cloud (tools para orquestração dos microservices)
     - eureka (service discovery/registration)
     - turbine (monitoring)
     - zuul (api proxy)
 - services (microservices)
     - campaign (micro serviço de campanhas)
     - fan (micro serviço de torcedores)
     - team (micro serviço de times, TODO)
    

DONE:
 - Arquitetura resiliente a falhas;
 - Mesmo que o micro serviço esteja indisponível um dado é retornado.
 - Toda a stack conteinerzada com docker.

TODO
 - Implementar testes,
 - Implementar persistencia dos dados,
 - Implementar lógica de negócios nas apis de team, fan e campaign.