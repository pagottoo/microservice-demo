package br.com.thiagopagotto.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Fan {

    private final String id;

    private final String fullName;

    private final String email;

    @JsonCreator
    public Fan(@JsonProperty("id") String id,
               @JsonProperty("fullName") String fullName,
               @JsonProperty("email") String email){
        this.id = id;
        this.fullName = fullName;
        this.email = email;

    }

    public String getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }
}
