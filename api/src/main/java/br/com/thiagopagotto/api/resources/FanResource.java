package br.com.thiagopagotto.api.resources;

import br.com.thiagopagotto.api.data.FanDataProvider;
import br.com.thiagopagotto.api.model.Fan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@RestController
public class FanResource {

    private static final Logger LOG = LoggerFactory.getLogger(FanResource.class);

    @Autowired
    private FanDataProvider fanDataProvider;

    @RequestMapping("/fan/{id}")
    public Fan getFan(@PathVariable String id) {
        Fan fan = null;

        try {
            return fanDataProvider.getFan(id).get();
        } catch (InterruptedException | ExecutionException e) {
            LOG.warn("Could not load Fan data",e);
        }
        return fan;
    }
}