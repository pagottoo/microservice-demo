package br.com.thiagopagotto.api.resources;

import br.com.thiagopagotto.api.data.CampaignDataProvider;
import br.com.thiagopagotto.api.model.Campaign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@RestController
public class CampaignResource {

    private static final Logger LOG = LoggerFactory.getLogger(CampaignResource.class);

    @Autowired
    private CampaignDataProvider campaignDataProvider;

    @RequestMapping("/campaign/{id}")
    public Campaign getCampaign(@PathVariable String id) {
        Campaign campaign = null;

        try {
            return campaignDataProvider.getCampaign(id).get();
        } catch (InterruptedException | ExecutionException e) {
            LOG.warn("Could not load Campaign data",e);
        }
        return campaign;
    }
}
