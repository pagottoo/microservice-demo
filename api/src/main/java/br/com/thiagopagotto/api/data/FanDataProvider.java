package br.com.thiagopagotto.api.data;

import br.com.thiagopagotto.api.model.Campaign;
import br.com.thiagopagotto.api.model.Fan;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.command.AsyncResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.Future;

@Component
public class FanDataProvider {

    private static final Logger LOG = LoggerFactory.getLogger(CampaignDataProvider.class);

    @Autowired
    private LoadBalancerClient loadBalancer;

    private RestTemplate restTemplate = new RestTemplate();

    @HystrixCommand(fallbackMethod = "defaultFan")
    public Future<Fan> getFan(String id) {
        return new AsyncResult<Fan>() {
            @Override
            public Fan invoke() {
                ServiceInstance instance = loadBalancer.choose("service-fan");
                if (instance != null) {
                    URI uri = instance.getUri();
                    final String url = uri.toString() + "/campaign/" + id;

                    LOG.info("Get Fan from '{}'",url);

                    ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

                    LOG.info("Status Code: '{}'",response.getStatusCodeValue());

                    ObjectMapper mapper = new ObjectMapper();

                    try {
                        return mapper.reader().forType(Campaign.class).readValue(response.getBody());
                    } catch (IOException e) {
                        LOG.warn("Unable to process Campaign response",e);
                    }
                }
                return defaultFan(id);
            }
        };
    }

    public Fan defaultFan(String id) {
        LOG.warn("Returning Default Fan");
        return new Fan(id,"Fan Default", "fan@default.com");
    }
}